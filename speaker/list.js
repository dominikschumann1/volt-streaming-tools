let men = [];
let women = [];
setInterval(() => {
    document.getElementsByClassName('men-list')[0].innerHTML = '<ul class="men-list"></ul>';
    document.getElementsByClassName('women-list')[0].innerHTML = ' <ul class="women-list"></ul>';
    document.getElementsByClassName('diverse-list')[0].innerHTML = ' <ul class="diverse-list"></ul>';
    fetch('/speaker/data').then((response) => {
        response.json().then((res) => {
            document.getElementById('title').innerText = 'Redeliste zu ' + res.antrag;
            document.getElementById('subtitle').innerText = res.subtitle ? res.subtitle : "";
            res.men.forEach((item) => {
                let li = document.createElement('li');
                document.getElementsByClassName('men-list')[0].appendChild(li);
            
                li.innerHTML += `<h4>${item.name}</h4>`;
            });
    
            res.women.forEach((item) => {
                let li = document.createElement('li');
                document.getElementsByClassName('women-list')[0].appendChild(li);
            
                li.innerHTML += `<h4>${item.name}</h4>`;
            });

            res.diverse.forEach((item) => {
                let li = document.createElement('li');
                document.getElementsByClassName('diverse-list')[0].appendChild(li);
            
                li.innerHTML += `<h4>${item.name}</h4>`;
            });
        });
    });
}, 5000)