const fs = require('fs');
const readline = require('readline');
const { google } = require('googleapis');
const path = require('path');
const express = require('express');
const crypto = require('crypto');
const axios = require('axios')
const app = express();

let spreadsheetLink = "";
let json = {yes: 0, no: 0};
let complexSpreadsheet = "1xYM8GkQJjj7llJwIK5r34av6Bg3JAxqG7MzBb5QcUK8";
let tuskerCode = "84";

const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly'];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = 'token.json';

let speakerResult = {
    women: [],
    men: [],
    diverse: [],
    antrag: ""
};

let voteResult = {
    votes: "",
    title: "",
    data: []
};

// Load client secrets from a local file.
fs.readFile('credentials.json', (err, content) => {
    if (err) return console.log('Error loading client secret file:', err);
    // Authorize a client with credentials, then call the Google Sheets API.
    authorize(JSON.parse(content), getSpeakers);
    authorize(JSON.parse(content), getVoteResult);
});

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
    const { client_secret, client_id, redirect_uris } = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
        client_id, client_secret, redirect_uris[0]);

    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, (err, token) => {
        if (err) return getNewToken(oAuth2Client, callback);
        oAuth2Client.setCredentials(JSON.parse(token));
        callback(oAuth2Client);
    });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url:', authUrl);
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', (code) => {
        rl.close();
        oAuth2Client.getToken(code, (err, token) => {
            if (err) return console.error('Error while trying to retrieve access token', err);
            oAuth2Client.setCredentials(token);
            // Store the token to disk for later program executions
            fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
                if (err) return console.error(err);
                console.log('Token stored to', TOKEN_PATH);
            });
            callback(oAuth2Client);
        });
    });
}

function getVoteResult(auth) {
    const sheets = google.sheets({ version: 'v4', auth });
    setInterval(() => {
        sheets.spreadsheets.values.get({
            spreadsheetId: complexSpreadsheet,
            range: 'A3:E',
        }, (err, res) => {
            if (err && complexSpreadsheet !== '') {
                voteResult.votes = "There is an Error with your Link. Please view an Administrator!"
                return console.log('The API returned an error: ' + err);
            }
            if (err && complexSpreadsheet === '') {
                voteResult.votes = "Please insert SpredsheetLink!";
                return;
            }
            if (!res.data || !res.data.values) {
                voteResult.data = [];
                return;
            }
            let rows = res.data.values;
            voteResult.votes = rows[0][2];
            voteResult.title = rows[0][3];

            let data = [];

            rows.map((row) => {
                let dataResult = {name: "", votes: ""};
                if (row[0] && row[0] != "") dataResult.name = row[0];
                if (row[1] && row[1] != "") dataResult.votes = row[1];
                dataResult.primary = (row[4] && row[4] != "") ? false : true;

                data.push(dataResult);
            });

            voteResult.data = data;
        });
    }, 10000);
}

function getSpeakers(auth) {
    const sheets = google.sheets({ version: 'v4', auth });
    setInterval(() => {
        sheets.spreadsheets.values.get({
            spreadsheetId: spreadsheetLink,
            range: 'A3:D',
        }, (err, res) => {
            if (err && spreadsheetLink !== '') {
                speakerResult.antrag = "There is an Error with your Link. Please view an Administrator!"
                return console.log('The API returned an error: ' + err);
            }
            if (err && spreadsheetLink === '') {
                speakerResult.antrag = "Please insert SpredsheetLink!";
                return;
            }
            if (!res.data || !res.data.values) {
                speakerResult.men = [];
                speakerResult.women = [];
                speakerResult.diverse = [];
                return;
            }
            let rows = res.data.values;
            speakerResult.antrag = rows[0][3];
            speakerResult.subtitle = rows[1] ? rows[1][3] ? rows[1][3] : "": "";

            let men = [];
            let women = [];
            let diverse = [];

            rows.map((row) => {
                if (row[0] && row[0] != "") men.push({ name: row[0] });
                if (row[1] && row[1] != "") women.push({ name: row[1] });
                if (row[2] && row[2] != "") diverse.push({ name: row[2] });
            });

            speakerResult.men = men;
            speakerResult.women = women;
            speakerResult.diverse = diverse;
        });
    }, 5000);
}

app.get('/speaker/data', (req, res) => {
    res.send(speakerResult);
});

app.use(express.static(path.join(__dirname, 'client')));

app.get('/speaker', (req, res) => {
    res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/speaker/list.js', (req, res) => {
    res.sendFile(path.join(__dirname, 'list.js'));
});

app.get('/speaker/config', (req, res) => {
    res.sendFile(path.join(__dirname, 'config.html'));
});

app.get('/speaker/resultData', (req, res) => {
    res.contentType('json');
    res.send(JSON.stringify(json));
});

app.get('/speaker/bars', (req, res) => {
    res.sendFile(path.join(__dirname, 'bars.html'))
})

app.get('/speaker/results', (req, res) => {
    res.sendFile(path.join(__dirname, 'complex-bars.html'))
})

app.get('/speaker/complexData', (req, res) => {
    res.contentType('json');
    res.send(JSON.stringify(voteResult));
})

app.get('/speaker/tuskerBar', (req, res) => {
    res.sendFile(path.join(__dirname, 'tusker-bars.html'));
})

app.get('/speaker/tuskerBar/data', async (req, res) => {
    const test = await axios.default.get('https://tusker.volteuropa.org/live_event/' + tuskerCode + '/info.json');
    res.send(test.data);
})

app.post('/speaker/config/submit', (req, res) => {
    const key = crypto.createHash('sha256').update(req.body.key).digest('base64');
    if (key !== "OUtYlKqIjffgD9TiJpBSyulCk7UoMwPpORALdnY6B0Q=") {
        res.status(403).send('');
        console.log("Unsuccessful Change Request");
    } else {
        spreadsheetLink = req.body.spreadsheetLink == "" ? spreadsheetLink : req.body.spreadsheetLink;
        complexSpreadsheet = req.body.complexBarsLink == "" ? complexSpreadsheet : req.body.complexBarsLink;
        tuskerCode = req.body.tuskerCode == "" ? tuskerCode : req.body.tuskerCode;
        json = {yes: req.body.barsYes == "" ? 0 : req.body.barsYes, no: req.body.barsNo == "" ? 0 : req.body.barsNo};
        console.log("Updated SpreadsheetLink to " + spreadsheetLink);
        res.status(303).setHeader("Location", "https://voltcharts.sillyscientists.de/speaker");
        res.send('');
    }
});

app.listen(6200, () => {
    console.log("Started Server on Port 6200");
});